package com.dji.FPVDemo;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class WebOverlord  {

    public class WebServer extends NanoHTTPD
    {
        public WebServer(int port)
        {
            super (port);
            try {
                start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public Response serve(String uri, Method method, Map<String, String> headers, Map<String, String> parms, Map<String, String> files) {
            Log.d(TAG, "httpd request >>" + method + " '" + uri + "' " + "   " + parms);

            String msg;

            targetAddr = headers.get("remote-addr");
            Log.d(TAG, "remote IP addr is "+targetAddr);

            streamingServer.inStreaming = true;
            if(uri.startsWith("/iframe"))
                iFramePending = true;

            msg  = "<html><head><title>DroneBridge</title>";
            if(!iFramePending) // if this is not an Iframe request, make it auto-refresh every 5 sec
                msg +="<meta http-equiv=\"refresh\" content=\"5\">";
            msg += "</head><body><h1>DroneBridge</h1>\nThe following gstreamer pipe works:<br>\n";
            msg += "gst-launch-1.0 udpsrc port=8088 ! h264parse ! avdec_h264 ! videoconvert ! xvimagesink sync=true<p>\n";

            msg += mDroneParams + "</p>\n";
            return newFixedLengthResponse( msg + "</body></html>\n" );

//            return newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Not Found");

//          return super.serve(uri, method, headers, parms, files);
        }
    }

    public static String TAG="WEB_OVERLORD";

    private final int ServerPort = 8080;
    private final int StreamingPort = 8088;
    private final int FlightDataPort = 8089;

    public String targetAddr=null;
    public boolean iFramePending;

    private StreamingServer streamingServer = null;
    private WebServer webServer = null;
    public String mDroneParams;

    ExecutorService executor = Executors.newFixedThreadPool(3);
    private ReentrantLock previewLock = new ReentrantLock();
    boolean inProcessing = false;

    public void init(Context context) {
        initWebServer(context);
        streamingServer = new StreamingServer();
    }

    public void SendVideo(byte []buffer, int size) {
        if (streamingServer !=null) {
            if (!streamingServer.started)
                streamingServer.start();
            if (streamingServer.started) {
                streamingServer.sendMedia(buffer, size, StreamingPort);
            }
        }
    }
    public void SendData(byte []buffer, int size) {
        // not opening or anything - do not send data before video is started
        if (streamingServer.started)
            streamingServer.sendMedia(buffer, size, FlightDataPort);
    }
    //
    //  Internal help functions
    //
    private boolean initWebServer(Context context) {

        String ipAddr = wifiIpAddress(context);
        if ( ipAddr != null ) {
            webServer = new WebServer(ServerPort);
            Log.d(TAG, "web server started on port "+ServerPort);
            return true;
        }
        else {
            Log.e(TAG, "no IP address, bailing out ");
            return false;
        }
    }

    protected String wifiIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
            Log.e(TAG, "got local address: "+ipAddressString);
        } catch (UnknownHostException ex) {
            Log.e("WIFIIP", "Unable to get my address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }

    private class StreamingServer {
        private InetAddress mTargetIP;
        private DatagramSocket mDatagramSocket=null;

        public boolean started = false;
        public boolean inStreaming = false;

        public void start() {
            if (started)
                return;
            if(targetAddr!=null)
            {
                try {
                    mTargetIP = InetAddress.getByName(targetAddr);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    mDatagramSocket = new DatagramSocket();
                    started = true;
                } catch (SocketException e) {
                    e.printStackTrace();
                }
            }
        }

        public void sendMedia(final byte[] message, final int length, final int port) {
            if ( started ) {
                new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            DatagramPacket p = new DatagramPacket(message, length, mTargetIP, port);
                            try {
                                mDatagramSocket.send(p);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            }
            else
                Log.d(TAG,"sendMedia called but inStreaming is false");
        }
    }
}
